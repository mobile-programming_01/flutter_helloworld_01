import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget{
  @override
  _HelloFlutterAppState createState() => _HelloFlutterAppState();
}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String japanGreeting = "OHAYÔ GOZAIMASU Flutter ";
String hawaiiGreeting = "Aloha Flutter";

class _HelloFlutterAppState extends State<HelloFlutterApp>{

  String displayText = englishGreeting;


  @override
  Widget  build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      //Scaffold Widget
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting?
                    spanishGreeting : englishGreeting;//if-else
                  });
                },
                icon: Icon(Icons.refresh)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting?
                    japanGreeting : englishGreeting;//if-else
                  });
                },
                icon: Icon(Icons.g_translate)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting?
                    hawaiiGreeting : englishGreeting;//if-else
                  });
                },
                icon: Icon(Icons.g_translate))
          ],
        ),
        body: Center(
          child: Text(
            displayText,
            style: TextStyle(fontSize: 24),
          ),
        ),
      ),
    );
  }
}


//safeArea
// class HelloFluterApp extends StatelessWidget{
//   @override
//   Widget  build(BuildContext context){
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//     //Scaffold Widget
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Hello Flutter"),
//           leading: Icon(Icons.home),
//           actions: <Widget>[
//             IconButton(onPressed: () {},
//                 icon: Icon(Icons.refresh))
//           ],
//         ),
//         body: Center(
//           child: Text(
//           "Hello flutter ! ",
//             style: TextStyle(fontSize: 24),
//         ),
//       ),
//       // home: SafeArea(
//       // child: Text("Hello Flutter"),
//     //   home: Center(
//     //     child: Text("Hello Flutter"),
//       ),
//     );
//   }
// }